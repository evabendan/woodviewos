<?php

add_action('vc_before_init', 'dental_care_gallery_grid_VC');

function dental_care_gallery_grid_VC() {
    vc_map(array(
        "name" => esc_html__("Gallery Grid", 'dental-care'),
        "base" => "dental_care_gallery_grid",
        "class" => "",
        "category" => esc_html__('Dental Care', 'dental-care'),
        "params" => array(
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Title", 'dental-care'),
                "param_name" => "title",
                "description" => esc_html__("Title text Here. Leave blank if no title is needed.", 'dental-care')
            ),
            array(
                "type" => "dropdown",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Number of Columns", 'dental-care'),
                "param_name" => "num_columns",
                "description" => esc_html__("Choose the number of columns to display", 'dental-care'),
                "value" => array(
                    'Two Columns' => 'two_columns',
                    'Three Columns' => 'three_col',
                    'Four Columns' => 'four_col',
                )
            ),
        )
    ));
}

function dental_care_gallery_grid_shortcode($atts, $content = NULL) {
    global $post;
    extract(shortcode_atts(array(
        'param' => '',
        'title' => '',
        'num_columns' => ''
                    ), $atts));

    $args = array(
        'type' => 'post',
        'child_of' => 0,
        'parent' => '',
        'orderby' => 'name',
        'order' => 'ASC',
        'hide_empty' => 0,
        'hierarchical' => 1,
        'number' => '9999',
        'taxonomy' => 'gallery-categories',
        'pad_counts' => false,
    );

    $categories = get_categories($args);
    if ($title != NULL && isset($title)) {
        $string = '<h3 class="dental-care-VC-title">' . esc_html($title) . '</h3>';
        $string .= '<div class="isotope-filter classcatFilter gallerycatFilter">';
    } else {
        $string = '<div class="isotope-filter classcatFilter">';
    }

    $string .= ' <a href="#" data-filter="*" class="current">' . esc_html__('All Galleries', 'dental-care') . '</a>';

    foreach ($categories as $cat) {
        $string .= '<a href="#" data-filter=".' . esc_attr($cat->slug) . '">' . esc_html($cat->name) . '</a>';
    }
    $string .= '</div>';

    $classargs = array(
        'post_type' => 'gallery',
        'post_status' => 'publish',
        'pagination' => true,
        'posts_per_page' => 9999
    );

    if ($num_columns == 'three_col') {
        $col_width = '33.3333333%';
        $title_font = '24px';
        $icon_font = '25px';
    } else if ($num_columns == 'four_col') {
        $col_width = '25%';
        $title_font = '16px';
        $icon_font = '18px';
    } else {
        $col_width = ' ';
        $title_font = '24px';
        $icon_font = '25px';
    }

    //The Query
    $query = new WP_Query($classargs);
    $string .= '<div class="isotope-cat-container">';
    while ($query->have_posts()) {
        $query->the_post();
        if (has_post_thumbnail()) {
            $gallerytitle = get_the_title();
            $gallerylink = get_the_permalink();
            $galleryimg = get_the_post_thumbnail($post->ID, 'dental-care-gallery-thumb');
            $term = wp_get_post_terms($post->ID, 'gallery-categories');

            $string .= '<div style="width:' . esc_attr($col_width) . ';" class="iso-cat-item ';
            foreach ($term as $val) {
                $string .= '' . esc_attr($val->slug) . '';
                $string .= ' ';
            }
            $string .= '">';
            $string .= '<div class="iso-cat-img-wrapper"><a href="' . esc_url($gallerylink) . '"><span class="iso-overlay"><span style="font-size:' . esc_attr($title_font) . ';" class="iso-overlay-title">' . esc_html($gallerytitle) . '</span> <i style="font-size:' . esc_attr($icon_font) . ';" class="fa fa-link"></i></span>' . $galleryimg . '</a></div>';
            $string .= '  </div>';
        }
    }
    $string .= '</div>';
wp_reset_postdata();
    return $string;
}
