<?php

include_once DENTALCARE_DIR . '/shortcodes/blog-carousel.php';
include_once DENTALCARE_DIR . '/shortcodes/blog-grid.php';
include_once DENTALCARE_DIR . '/shortcodes/brands.php';
include_once DENTALCARE_DIR . '/shortcodes/counter.php';
include_once DENTALCARE_DIR . '/shortcodes/gallery-grid.php';
include_once DENTALCARE_DIR . '/shortcodes/icon-box.php';
include_once DENTALCARE_DIR . '/shortcodes/info-icon.php';
include_once DENTALCARE_DIR . '/shortcodes/opening-hours.php';
include_once DENTALCARE_DIR . '/shortcodes/price-list.php';
include_once DENTALCARE_DIR . '/shortcodes/product-carousel.php';
include_once DENTALCARE_DIR . '/shortcodes/single-gallery.php';
include_once DENTALCARE_DIR . '/shortcodes/services.php';
include_once DENTALCARE_DIR . '/shortcodes/team-members.php';
include_once DENTALCARE_DIR . '/shortcodes/testimonials.php';


function register_shortcodes() {
    add_shortcode('dental_care_blog_carousel', 'dental_care_blog_carousel_shortcode');
    add_shortcode('dental_care_blog_grid', 'dental_care_blog_grid_shortcode');
    add_shortcode('dental_care_brands', 'dental_care_brands_shortcode');
    add_shortcode('dental_care_gallery_grid', 'dental_care_gallery_grid_shortcode');
    add_shortcode('dental_care_single_gallery', 'dental_care_single_gallery_shortcode');
    add_shortcode('dental_care_services', 'dental_care_services_shortcode');
    add_shortcode('dental_care_team_members', 'dental_care_team_members_shortcode');
    add_shortcode('dental_care_opening_hours', 'dental_care_opening_hours_shortcode');
    add_shortcode('dental_care_price_list', 'dental_care_price_list_shortcode');
    add_shortcode('dental_care_shop_carousel', 'dental_care_shop_shortcode');
    add_shortcode('dental_care_testimonials', 'dental_care_testimonial_shortcode');   
    add_shortcode('dental_care_info_icon', 'dental_care_info_icon_shortcode');
    add_shortcode('dental_care_icon_box', 'dental_care_icon_box_shortcode');
    add_shortcode('dental_care_counter', 'dental_care_counter_shortcode');

}

add_action('init', 'register_shortcodes');

