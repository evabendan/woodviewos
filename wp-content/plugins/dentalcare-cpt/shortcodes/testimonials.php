<?php

add_action('vc_before_init', 'dental_care_testimonial_VC');

function dental_care_testimonial_VC() {

    vc_map(array(
        "name" => esc_html__("Testimonials", 'dental-care'),
        "base" => "dental_care_testimonials",
        "class" => "",
        "category" => esc_html__('Dental Care', 'dental-care'),
        "params" => array(
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Title", 'dental-care'),
                "param_name" => "title",
                "description" => esc_html__("Title text Here. Leave blank if no title is needed.", 'dental-care')
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Carousel Speed", 'dental-care'),
                "param_name" => "carousel_speed",
                "description" => esc_html__("Enter the number for the carousel speed. (Default: 5000)", 'dental-care')
            ),
            array(
                "type" => "dropdown",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Order by title", 'dental-care'),
                "param_name" => "order_items",
                "description" => esc_html__("Choose if to order items", 'dental-care'),
                "value" => array(
                    '' => '',
                    'Yes' => 'yes',
                    'No' => 'no',
                )
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Number of carousel items", 'dental-care'),
                "param_name" => "carousel_items",
                "description" => esc_html__("Enter the number of testimonials columns to display in carousel.", 'dental-care')
            ),
            array(
                "type" => "dropdown",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Enable Arrows", 'dental-care'),
                "param_name" => "arrows_en",
                "description" => esc_html__("Choose to enable or disable arrows on carousel.", 'dental-care'),
                "value" => array(
                    '' => '',
                    'On' => 'on',
                    'Off' => 'off',
                ),
                
            ),
        )
    ));
}

function dental_care_testimonial_shortcode($atts, $content = NULL) {
    global $post;
    extract(shortcode_atts(array(
        'param' => '',
        'title' => '',
        'carousel_speed' => '',
        'carousel_items' => '',
        'order_items' => '',
        'arrows_en' => '',
                    ), $atts));
    
     if ($order_items == 'yes'){       
         $args = array(
        'post_type' => 'testimonial',
        'post_status' => 'publish',
        'pagination' => true,
        'orderby' => 'title',
	'order'   => 'ASC',
        'posts_per_page' => -1,   
    );
    }else{
     $args = array(
        'post_type' => 'testimonial',
        'post_status' => 'publish',
        'pagination' => true,
        'posts_per_page' => -1
    );
    }    

    $allowed_html = array(
        'p' => array(
        'class' => array(),
        'style' => array(),
    )
    );

    // The Query
    $query = new WP_Query($args);

    $string = '<div class="dental-care-testimonials-wrapper">';
    
    if ($arrows_en == 'on') {       
            $string .= '<div class="carousel_arrow_nav_top">';           
            $string .= '<a class="btn arrow_prev_top"><i class="fa fa-chevron-left"></i></a>';
            $string .= '<a class="btn arrow_next_top"><i class="fa fa-chevron-right"></i></a>';
            $string .= '</div>';       
    }

    if ($title != NULL) {
        $string .= '<h3 class="dental-care-VC-title">' . esc_html($title) . '</h3>';
    }
    $string .= '<ul class="dental-care-testimonials owl-carousel">';

    $postcount = $query->post_count;
    dental_care_add_testi_carousel($carousel_speed, $carousel_items, $postcount);


    while ($query->have_posts()) {
        $query->the_post();

        $testimonytext = "" . get_post_meta($post->ID, 'testimonialtext', $single = true);
        $testimonyname = get_post_meta($post->ID, 'testimonialname', $single = true);
        $testimonypos = get_post_meta($post->ID, 'testimonialposition', $single = true);
        $testimonypic = get_the_post_thumbnail($post->ID, 'thumbnail');

        $string .= '<li class="dental-care-testimonials-item"> <i class="fa fa-quote-left"></i><div class="dental-care-testim-text">' . wp_kses($testimonytext, $allowed_html) . ' </div>';
        $string .= '<div class="dental-care-author">';
        if ($testimonypic != NULL) {
            $string .= $testimonypic;
        }
        $string .= '<ul class="dental-care-author-info">  <li class="dental-care-testim-name">' . esc_html($testimonyname) . '</li> ';
        $string .= '<li class="dental-care-testim-position">' . esc_html($testimonypos) . '</li>  </ul> </div>   </li> ';
    }

    $string .= '</ul></div>';
    wp_reset_postdata();
    return $string;
}

/**
 * Add Carousel Settings
 */
function dental_care_add_testi_carousel($carousel_speed = NULL, $carousel_items = 2, $postcount = NULL) {
    echo '<script>';
    echo 'jQuery(document).ready(function($) {';
    echo '"use strict";';
    echo '$(".dental-care-testimonials").owlCarousel({';
    if ($postcount > 1) {
        echo 'loop: true,';
    } else {
        echo 'loop: false,';
    }
    echo ' 
            margin: 30,
            autoplay: true,';
    if ($carousel_speed == NULL || !isset($carousel_speed)) {
        echo 'autoplayTimeout: 5000,';
    } else {
        echo 'autoplayTimeout: ' . esc_html($carousel_speed) . ',';
    }
    echo'
            navigation: false,
            dots: false,
            items: ' . esc_html($carousel_items) . ',
            responsiveClass:true,
            responsive:{
            0:{
            items:1,           
             },
            800:{
            items:2,            
            },
            1100:{
            items:' . esc_html($carousel_items) . ', 
            }
            }
                });
                
     $(".dental-care-testimonials-wrapper .arrow_next_top").click(function(){
     $(".dental-care-testimonials").trigger("next.owl.carousel");
     })
     $(".dental-care-testimonials-wrapper .arrow_prev_top").click(function(){
     $(".dental-care-testimonials").trigger("prev.owl.carousel");
     })
     
    });';
    echo '</script>';
}

add_action('wp_footer', 'dental_care_add_testi_carousel', 10, 2);

