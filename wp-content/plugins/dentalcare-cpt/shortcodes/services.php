<?php

add_action('vc_before_init', 'dental_care_services_VC');

function dental_care_services_VC() {
    
    // class_category_field
    function service_category_field_list($settings, $value) {

        $categories = get_terms('service-categories');

        $dependency = vc_generate_dependencies_attributes($settings);

        $data = '<select name="' . $settings['param_name'] . '" class="wpb_vc_param_value wpb-input wpb-select ' . $settings['param_name'] . ' ' . $settings['type'] . '">';

        foreach ($categories as $category) {
            $selected = '';
            if ($value !== '' && $category->slug === $value) {
                $selected = ' selected="selected"';
            }
            $data .= '<option value="' . $category->slug . '"' . $selected . '>' . $category->name . ' </option>';
        }
        $data .= '</select>';
        return $data;
    }

    add_shortcode_param('service_category', 'service_category_field_list');
  
    
    vc_map(array(
        "name" => esc_html__("Services", 'dental-care'),
        "base" => "dental_care_services",
        "class" => "",
        "category" => esc_html__('Dental Care', 'dental-care'),
        "params" => array(
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Title", 'dental-care'),
                "param_name" => "title",
                "description" => esc_html__("Title text Here. Leave blank if no title is needed.", 'dental-care')
            ),
            array(
                "type" => "dropdown",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Service Display Type", 'dental-care'),
                "param_name" => "service_type",
                "description" => esc_html__("Choose a service type.", 'dental-care'),
                "value" => array("" => "", "Services Carousel" => "service_carousel", "Services Grid" => "service_grid"),
            ),
            array(
                "type" => "dropdown",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Order by title", 'dental-care'),
                "param_name" => "order_items",
                "description" => esc_html__("Choose if to order items", 'dental-care'),
                "value" => array(
                    '' => '',
                    'Yes' => 'yes',
                    'No' => 'no',
                )
            ),
            array(
                "type" => "dropdown",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Enable services links", 'dental-care'),
                "param_name" => "links_en",
                "description" => esc_html__("Choose to enable services links.", 'dental-care'),
                "value" => array(
                    '' => '',
                    'On' => 'on',
                    'Off' => 'off',
                )
            ),
            array(
                "type" => "dropdown",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Filter services by category", 'dental-care'),
                "param_name" => "filter_services_en",
                "description" => esc_html__("Choose to filter services by category.", 'dental-care'),
                "value" => array(
                    '' => '',
                    'On' => 'on',
                    'Off' => 'off',
                )
            ),
            array(
                "type" => "service_category",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Service Category", 'dental-care'),
                "param_name" => "service_category",
                "description" => esc_html__("Choose a service category.", 'dental-care'),
                "dependency" => array("element" => "filter_services_en", "value" => array("on")),
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Number of items", 'dental-care'),
                "param_name" => "num_items",
                "description" => esc_html__("Enter the number of services to display. Enter -1 to display all items.", 'dental-care')
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Carousel Speed", 'dental-care'),
                "param_name" => "carousel_speed",
                "description" => esc_html__("Enter the number for the carousel speed. (Default: 5000)", 'dental-care')
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Number of carousel items", 'dental-care'),
                "param_name" => "carousel_items",
                "description" => esc_html__("Enter the number of services columns to display in carousel.", 'dental-care')
            ),
            array(
                "type" => "dropdown",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Enable Arrows", 'dental-care'),
                "param_name" => "arrows_en",
                "description" => esc_html__("Choose to enable or disable arrows on carousel.", 'dental-care'),
                "value" => array(
                    '' => '',
                    'On' => 'on',
                    'Off' => 'off',
                ),
                
            ),
        )
    ));
}

function dental_care_services_shortcode($atts, $content = NULL) {
    global $post;
    extract(shortcode_atts(array(
        'param' => '',
        'title' => '',
        'num_items' => ' ',
        'carousel_items' => '',
        'service_type' => '',
        'carousel_speed' => '',
        'order_items' => '',
        'links_en' => '',
        'service_category' => '',
        'arrows_en' => '',
                    ), $atts));

    if ($num_items == NULL) {
        $num_items = -1;
    }
    
    if ($service_category == NULL) {
    if ($order_items == 'yes'){
        $args = array(
        'post_type' => 'service',
        'post_status' => 'publish',
        'pagination' => true,
        'orderby' => 'title',
	'order'   => 'ASC',
        'posts_per_page' => $num_items,
    );
    }else{

    $args = array(
        'post_type' => 'service',
        'post_status' => 'publish',
        'pagination' => true,
        'posts_per_page' => $num_items,
    );
    }
    }else{
        if ($order_items == 'yes'){
        $args = array(
        'post_type' => 'service',
        'post_status' => 'publish',
        'pagination' => true,
        'orderby' => 'title',
	'order'   => 'ASC',
        'posts_per_page' => $num_items,
        'tax_query' => array(
                    array(
                        'taxonomy' => 'service-categories',
                        'field' => 'slug',
                        'terms' => $service_category,
                    ),
                ),
    );
    }else{

    $args = array(
        'post_type' => 'service',
        'post_status' => 'publish',
        'pagination' => true,
        'posts_per_page' => $num_items,
        'tax_query' => array(
                    array(
                        'taxonomy' => 'service-categories',
                        'field' => 'slug',
                        'terms' => $service_category,
                    ),
                ),
    );
    }
    }
    
    $allowed_html = array(
    'abbr' => array(
        'title' => true,
    ),
    'acronym' => array(
        'title' => true,
    ),
    'b' => array(),
    'blockquote' => array(
        'cite' => true,
    ),
    'cite' => array(),
    'code' => array(),
    'em' => array(),
    'i' => array(),
    'q' => array(
        'cite' => true,
    ),
    'strike' => array(),
    'strong' => array(),
    'i' => array(
        'class' => array(),
        'title' => array(),
        'style' => array(),
    ),
    'a' => array(
        'href' => array(),
        'rel' => array(),
        'class' => array(),
        'style' => array(),
    ),
    'p' => array(
        'class' => array(),
        'style' => array(),
    ),
    'ul' => array(
        'class' => array(),
        'style' => array(),
    ),
    'ol' => array(
        'class' => array(),
        'style' => array(),
    ),
    'li' => array(
        'class' => array(),
        'style' => array(),
    )
);

    // The Query
    $query = new WP_Query($args);

    $string = '<div class="dental-care-services-wrapper">';
    
    if ($arrows_en == 'on') {       
            $string .= '<div class="carousel_arrow_nav_top">';           
            $string .= '<a class="btn arrow_prev_top"><i class="fa fa-chevron-left"></i></a>';
            $string .= '<a class="btn arrow_next_top"><i class="fa fa-chevron-right"></i></a>';
            $string .= '</div>';       
    }

    if ($title != NULL) {
        $string .= '<h3 class="dental-care-VC-title">' . esc_html($title) . '</h3>';
    }

    if ($service_type == "service_carousel") {

        $postcount = $query->post_count;
        dental_care_add_services_carousel($carousel_speed, $carousel_items, $postcount);

        $string .= '<div class="dental-care-service-carousel owl-carousel">';
        while ($query->have_posts()) {
            $query->the_post();
            $dental_care_service_desc = get_post_meta($post->ID, 'service_desc', $single = true);
            $service_img = get_the_post_thumbnail($post->ID, 'dental-care-block-thumb');

            $string .= '<div class="service-block">';
            if ($service_img != NULL){
            $string .='<div class="service-block-img">';
            if ($links_en == "on" || $links_en == "") {
            $string .= '<a rel="external" href="' . get_the_permalink() . '">' . $service_img . '</a>';
            $string .= '<a rel="external" href="' . get_the_permalink() . '">';
            }else{
            $string .= $service_img ;   
            }
            $string .= '<span class="service-block-img-overlay">';
            $string .= '<i class="fa fa-link"></i>';
            $string .= '</span></a></div>';
            }
            $string .= '<div class="service-main-detail">';
            if ($links_en == "on" || $links_en == "") {
            $string .= '<h5 class="service-main-name"><a href="' . get_the_permalink() . '">' . get_the_title() . '</a></h5>';
            }else{
            $string .= '<h5 class="service-main-name">' . get_the_title() . '</h5>';   
            }
            $string .= '<div class="service-desc">' . wp_kses($dental_care_service_desc, $allowed_html) . '</div>';
            $string .= '</div>';
            $string .= '</div>';
        }
        $string .= '</div>';
    } else if ($service_type == "service_grid") {
        $services_count = 3;
        while ($query->have_posts()) {
            $query->the_post();
            $dental_care_service_desc = get_post_meta($post->ID, 'service_desc', $single = true);
            $service_img = get_the_post_thumbnail($post->ID, 'dental-care-block-thumb');
            if ($services_count == 3) {
                $services_count = 0;
                $string .= '<div class="row">';
            }
            $services_count++;

            $string .= '<div class="col-md-4 service-block-block-item">';
            $string .= '<div class="service-block">';
            if ($service_img != NULL){
            $string .= '<div class="service-block-img">';
            if ($links_en == "on" || $links_en == "") {
            $string .= '<a rel="external" href="' . get_the_permalink() . '">' . $service_img . '</a>';
            $string .= '<a rel="external" href="' . get_the_permalink() . '">';
            }else{
            $string .= $service_img ;   
            }
            $string .= '<span class="service-block-img-overlay"><i class="fa fa-link"></i></span>';
            $string .= '</a></div>';
            }
            $string .= '<div class="service-main-detail">';
            if ($links_en == "on" || $links_en == "") {
                $string .= '<h5 class="service-main-name"><a href="' . get_the_permalink() . '">' . get_the_title() . '</a></h5>';
            }else{
                $string .= '<h5 class="service-main-name">' . get_the_title() . '</h5>';
            }
            $string .= '<div class="service-desc">' . wp_kses($dental_care_service_desc, $allowed_html) . '</div>';
            $string .= '</div> </div></div>';
            if ($services_count == 3) {
                $string .= '</div>';
            }
            }
            if ($services_count < 3) {
            $string .= '</div>';
            }
    }
wp_reset_postdata();
    $string .= '</div> ';
    return $string;
}

/**
 * Add Carousel Settings
 */
function dental_care_add_services_carousel($carousel_speed = NULL, $carousel_items = 3, $postcount = NULL) {
    echo '<script>';
    echo 'jQuery(document).ready(function($) {';
    echo '"use strict";';
    echo '$(".dental-care-service-carousel").owlCarousel({';
    if ($postcount > 1) {
        echo 'loop: true,';
    } else {
        echo 'loop: false,';
    }
    echo ' 
            margin: 30,
            autoplay: true,';
    if ($carousel_speed == NULL || !isset($carousel_speed)) {
        echo 'autoplayTimeout: 5000,';
    } else {
        echo 'autoplayTimeout: ' . esc_html($carousel_speed) . ',';
    }
    echo'
            navigation: false,
            dots: false,
            autoplayHoverPause: true,
            items: ' . esc_html($carousel_items) . ',
            responsiveClass:true,
            responsive:{
            0:{
            items:1,           
             },
            800:{
            items:2,            
            },
            1100:{
            items:' . esc_html($carousel_items) . ', 
            }
            }
                });
    
     $(".dental-care-services-wrapper .arrow_next_top").click(function(){
     $(".dental-care-service-carousel").trigger("next.owl.carousel");
     })
     $(".dental-care-services-wrapper .arrow_prev_top").click(function(){
     $(".dental-care-service-carousel").trigger("prev.owl.carousel");
     })
  
    });';
    echo '</script>';
}

add_action('wp_footer', 'dental_care_add_services_carousel', 10, 2);
