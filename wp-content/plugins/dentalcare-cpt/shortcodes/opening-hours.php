<?php

add_action('vc_before_init', 'dental_care_opening_hours_VC');

function dental_care_opening_hours_VC() {
    vc_map(array(
        "name" => esc_html__("Opening Hours", 'dental-care'),
        "base" => "dental_care_opening_hours",
        "class" => "",
        "category" => esc_html__('Dental Care', 'dental-care'),
        "params" => array(
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Monday Title", 'dental-care'),
                "param_name" => "monday_title",
                "description" => esc_html__("Enter the title text for Monday", 'dental-care')
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Monday Text", 'dental-care'),
                "param_name" => "monday_text",
                "description" => esc_html__("Enter the opening hours for Monday", 'dental-care')
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Tuesday Title", 'dental-care'),
                "param_name" => "tuesday_title",
                "description" => esc_html__("Enter the title text for Tuesday", 'dental-care')
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Tuesday Text", 'dental-care'),
                "param_name" => "tuesday_text",
                "description" => esc_html__("Enter the opening hours for Tuesday", 'dental-care')
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Wednesday Title", 'dental-care'),
                "param_name" => "wednesday_title",
                "description" => esc_html__("Enter the title text for Wednesday", 'dental-care')
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Wednesday Text", 'dental-care'),
                "param_name" => "wednesday_text",
                "description" => esc_html__("Enter the opening hours for Wednesday", 'dental-care')
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Thursday Title", 'dental-care'),
                "param_name" => "thursday_title",
                "description" => esc_html__("Enter the title text for Thursday", 'dental-care')
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Thursday Text", 'dental-care'),
                "param_name" => "thursday_text",
                "description" => esc_html__("Enter the opening hours for Thursday", 'dental-care')
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Friday Title", 'dental-care'),
                "param_name" => "friday_title",
                "description" => esc_html__("Enter the title text for Friday", 'dental-care')
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Friday Text", 'dental-care'),
                "param_name" => "friday_text",
                "description" => esc_html__("Enter the opening hours for Friday", 'dental-care')
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Saturday Title", 'dental-care'),
                "param_name" => "saturday_title",
                "description" => esc_html__("Enter the title text for Saturday", 'dental-care')
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Saturday Text", 'dental-care'),
                "param_name" => "saturday_text",
                "description" => esc_html__("Enter the opening hours for Saturday", 'dental-care')
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Sunday Title", 'dental-care'),
                "param_name" => "sunday_title",
                "description" => esc_html__("Enter the title text for Sunday", 'dental-care')
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Sunday Text", 'dental-care'),
                "param_name" => "sunday_text",
                "description" => esc_html__("Enter the opening hours for Sunday", 'dental-care')
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Icon", 'dental-care'),
                "param_name" => "opening_hours_icon",
                "description" => esc_html__("Enter an icon class. Also supports Font Awesome e.g. fa fa-check., See icons", "dental-care") . " <a href='https://fortawesome.github.io/Font-Awesome/icons/' target='_blank'>here</a>",
                "group" => "Design"
                ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Icon Font Size", 'dental-care'),
                "param_name" => "opening_hours_icon_size",
                "min" => 1,
                "max" => 100,
                "suffix" => "px",
                "description" => esc_html__("Enter an icon font size.", 'dental-care'),
                "group" => "Design"
            ),
            array(
                "type" => "colorpicker",
                "class" => "",
                "heading" => esc_html__("Icon Color", 'dental-care'),
                "param_name" => "opening_hours_icon_color",
                "description" => esc_html__("Choose an icon color color.", 'dental-care'),
                "group" => "Design"
            ),
            array(
                "type" => "colorpicker",
                "class" => "",
                "heading" => esc_html__("Background Color", 'dental-care'),
                "param_name" => "opening_hours_bg_color",
                "description" => esc_html__("Choose a background color or combine it with an image as an overlay.", 'dental-care'),
                "group" => "Design"
            ),
            array(
                "type" => "attach_image",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Background Image", 'dental-care'),
                "param_name" => "opening_hours_bg_img",
                "description" => esc_html__("Choose an image for the background.", 'dental-care'),
                "group" => "Design"
            ),
            array(
                "type" => "colorpicker",
                "class" => "",
                "heading" => esc_html__("Text Color", 'dental-care'),
                "param_name" => "opening_hours_text_color",
                "description" => esc_html__("Choose a text color.", 'dental-care'),
                "group" => "Design"
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Title Font Size", 'dental-care'),
                "param_name" => "opening_hours_title_size",
                "min" => 1,
                "max" => 100,
                "suffix" => "px",
                "description" => esc_html__("Enter title font size.", 'dental-care'),
                "group" => "Typography"
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Opening Hours Font Size", 'dental-care'),
                "param_name" => "opening_hours_hours_size",
                "min" => 1,
                "max" => 100,
                "suffix" => "px",
                "description" => esc_html__("Enter opening hours font size.", 'dental-care'),
                "group" => "Typography"
            ),
        )
    ));
}

function dental_care_opening_hours_shortcode($atts, $content = NULL) {
    global $post;
    extract(shortcode_atts(array(
        'param' => '',
        'monday_title' => '',
        'monday_text' => '',
        'tuesday_title' => '',
        'tuesday_text' => '',
        'wednesday_title' => '',
        'wednesday_text' => '',
        'thursday_title' => '',
        'thursday_text' => '',
        'friday_title' => '',
        'friday_text' => '',
        'saturday_title' => '',
        'saturday_text' => '',
        'sunday_title' => '',
        'sunday_text' => '',
        'opening_hours_bg_color' => '',
        'opening_hours_bg_img' => '',
        'opening_hours_text_color' => '',
        'opening_hours_title_size' => '',
        'opening_hours_hours_size' => '',
        'opening_hours_icon' => '',
        'opening_hours_icon_size' => '',
        'opening_hours_icon_color' => ''
                    ), $atts));

    $opening_hours_bg_img_src = '';
    if ($opening_hours_bg_img != '') {
        $opening_hours_bg_img_src = wp_get_attachment_url($opening_hours_bg_img, 'full', false, false);
    }

    $string = '<div class="dental-care-opening-hours-widget" style="';
   
    if ($opening_hours_bg_color != ''){
        $string .= 'background:linear-gradient(
      ' . esc_attr($opening_hours_bg_color) . ', 
      ' . esc_attr($opening_hours_bg_color) . '
    ) ';
        if ($opening_hours_bg_img_src != '') {
            $string .= ',url(' . esc_url($opening_hours_bg_img_src) . ') no-repeat center center; background-size:cover;';
        } else {
            $string .= ';';
        }

    }
   
    $string .='">';
    
    if(($monday_title && $monday_text) != ""){
        
    $string .='<div class="opening-hours-item">';
    
    if($opening_hours_icon != ''){
            $string .= '<i class="opening-hours-icon ' .esc_attr($opening_hours_icon).'" style="';
            
            if ($opening_hours_icon_color != "") {
            $string .= ' color:' . esc_attr($opening_hours_icon_color) . ';';
            }
            if ($opening_hours_icon_size != "") {
            $string .= ' font-size:' . esc_attr($opening_hours_icon_size) . 'px;';
            }
            
            $string .= '"></i>';
        }
    
    $string .='<div class="opening-hours-day" style="';
    
    if($opening_hours_text_color != ""){
        $string .= ' color:'.esc_attr($opening_hours_text_color).';';
    }
    if($opening_hours_title_size != ""){
        $string .= ' font-size:'.esc_attr($opening_hours_title_size).'px;';
    }
    
    $string .='">';
    $string .='<span>'.esc_html($monday_title).'</span>';
    $string .='</div>';   
    $string .='<div class="opening-hours-time"  style="';
    
    if($opening_hours_text_color != ""){
        $string .= ' color:'.esc_attr($opening_hours_text_color).';';
    }
    if($opening_hours_hours_size != ""){
        $string .= ' font-size:'.esc_attr($opening_hours_hours_size).'px;';
    }
    
    $string .='">';
    $string .='<span>'.esc_html($monday_text).'</span>';
    $string .='</div>';
    $string .='</div>';
    }
    
    if(($tuesday_title && $tuesday_text) != ""){
    $string .='<div class="opening-hours-item">';
    
    if($opening_hours_icon != ''){
            $string .= '<i class="opening-hours-icon ' .esc_attr($opening_hours_icon).'" style="';
            
            if ($opening_hours_icon_color != "") {
            $string .= ' color:' . esc_attr($opening_hours_icon_color) . ';';
            }
            if ($opening_hours_icon_size != "") {
            $string .= ' font-size:' . esc_attr($opening_hours_icon_size) . 'px;';
            }
            
            $string .= '"></i>';
        }
    
    $string .='<div class="opening-hours-day" style="';
    
    if($opening_hours_text_color != ""){
        $string .= ' color:'.esc_attr($opening_hours_text_color).';';
    }
    if($opening_hours_title_size != ""){
        $string .= ' font-size:'.esc_attr($opening_hours_title_size).'px;';
    }
    
    $string .='">';
    $string .='<span>'.esc_html($tuesday_title).'</span>';
    $string .='</div>';   
    $string .='<div class="opening-hours-time"  style="';
    
    if($opening_hours_text_color != ""){
        $string .= ' color:'.esc_attr($opening_hours_text_color).';';
    }
    if($opening_hours_hours_size != ""){
        $string .= ' font-size:'.esc_attr($opening_hours_hours_size).'px;';
    }
    
    $string .='">';
    $string .='<span>'.esc_html($tuesday_text).'</span>';
    $string .='</div>';
    $string .='</div>';
    }
    if(($wednesday_title && $wednesday_text) != ""){
    $string .='<div class="opening-hours-item">';
    
    if($opening_hours_icon != ''){
            $string .= '<i class="opening-hours-icon ' .esc_attr($opening_hours_icon).'" style="';
            
            if ($opening_hours_icon_color != "") {
            $string .= ' color:' . esc_attr($opening_hours_icon_color) . ';';
            }
            if ($opening_hours_icon_size != "") {
            $string .= ' font-size:' . esc_attr($opening_hours_icon_size) . 'px;';
            }
            
            $string .= '"></i>';
        }
    
    $string .='<div class="opening-hours-day" style="';
    
    if($opening_hours_text_color != ""){
        $string .= ' color:'.esc_attr($opening_hours_text_color).';';
    }
    if($opening_hours_title_size != ""){
        $string .= ' font-size:'.esc_attr($opening_hours_title_size).'px;';
    }
    
    $string .='">';
    $string .='<span>'.esc_html($wednesday_title).'</span>';
    $string .='</div>';   
    $string .='<div class="opening-hours-time"  style="';
    
    if($opening_hours_text_color != ""){
        $string .= ' color:'.esc_attr($opening_hours_text_color).';';
    }
    if($opening_hours_hours_size != ""){
        $string .= ' font-size:'.esc_attr($opening_hours_hours_size).'px;';
    }
    
    $string .='">';
    $string .='<span>'.esc_html($wednesday_text).'</span>';
    $string .='</div>';
    $string .='</div>';
    }
    if(($thursday_title && $thursday_text) != ""){
    $string .='<div class="opening-hours-item">';
    
    if($opening_hours_icon != ''){
            $string .= '<i class="opening-hours-icon ' .esc_attr($opening_hours_icon).'" style="';
            
            if ($opening_hours_icon_color != "") {
            $string .= ' color:' . esc_attr($opening_hours_icon_color) . ';';
            }
            if ($opening_hours_icon_size != "") {
            $string .= ' font-size:' . esc_attr($opening_hours_icon_size) . 'px;';
            }
            
            $string .= '"></i>';
        }
    
    $string .='<div class="opening-hours-day" style="';
    
    if($opening_hours_text_color != ""){
        $string .= ' color:'.esc_attr($opening_hours_text_color).';';
    }
    if($opening_hours_title_size != ""){
        $string .= ' font-size:'.esc_attr($opening_hours_title_size).'px;';
    }
    
    $string .='">';
    $string .='<span>'.esc_html($thursday_title).'</span>';
    $string .='</div>';   
    $string .='<div class="opening-hours-time"  style="';
    
    if($opening_hours_text_color != ""){
        $string .= ' color:'.esc_attr($opening_hours_text_color).';';
    }
    if($opening_hours_hours_size != ""){
        $string .= ' font-size:'.esc_attr($opening_hours_hours_size).'px;';
    }
    
    $string .='">';
    $string .='<span>'.esc_html($thursday_text).'</span>';
    $string .='</div>';
    $string .='</div>';
    }
    if(($friday_title && $friday_text) != ""){
    $string .='<div class="opening-hours-item">';
    
    if($opening_hours_icon != ''){
            $string .= '<i class="opening-hours-icon ' .esc_attr($opening_hours_icon).'" style="';
            
            if ($opening_hours_icon_color != "") {
            $string .= ' color:' . esc_attr($opening_hours_icon_color) . ';';
            }
            if ($opening_hours_icon_size != "") {
            $string .= ' font-size:' . esc_attr($opening_hours_icon_size) . 'px;';
            }
            
            $string .= '"></i>';
        }
    
    $string .='<div class="opening-hours-day" style="';
    
    if($opening_hours_text_color != ""){
        $string .= ' color:'.esc_attr($opening_hours_text_color).';';
    }
    if($opening_hours_title_size != ""){
        $string .= ' font-size:'.esc_attr($opening_hours_title_size).'px;';
    }
    
    $string .='">';
    $string .='<span>'.esc_html($friday_title).'</span>';
    $string .='</div>';   
    $string .='<div class="opening-hours-time"  style="';
    
    if($opening_hours_text_color != ""){
        $string .= ' color:'.esc_attr($opening_hours_text_color).';';
    }
    if($opening_hours_hours_size != ""){
        $string .= ' font-size:'.esc_attr($opening_hours_hours_size).'px;';
    }
    
    $string .='">';
    $string .='<span>'.esc_html($friday_text).'</span>';
    $string .='</div>';
    $string .='</div>';
    }
    if(($saturday_title && $saturday_text) != ""){
    $string .='<div class="opening-hours-item">';
    
    if($opening_hours_icon != ''){
            $string .= '<i class="opening-hours-icon ' .esc_attr($opening_hours_icon).'" style="';
            
            if ($opening_hours_icon_color != "") {
            $string .= ' color:' . esc_attr($opening_hours_icon_color) . ';';
            }
            if ($opening_hours_icon_size != "") {
            $string .= ' font-size:' . esc_attr($opening_hours_icon_size) . 'px;';
            }
            
            $string .= '"></i>';
        }
    
    $string .='<div class="opening-hours-day" style="';
    
    if($opening_hours_text_color != ""){
        $string .= ' color:'.esc_attr($opening_hours_text_color).';';
    }
    if($opening_hours_title_size != ""){
        $string .= ' font-size:'.esc_attr($opening_hours_title_size).'px;';
    }
    
    $string .='">';
    $string .='<span>'.esc_html($saturday_title).'</span>';
    $string .='</div>';   
    $string .='<div class="opening-hours-time"  style="';
    
    if($opening_hours_text_color != ""){
        $string .= ' color:'.esc_attr($opening_hours_text_color).';';
    }
    if($opening_hours_hours_size != ""){
        $string .= ' font-size:'.esc_attr($opening_hours_hours_size).'px;';
    }
    
    $string .='">';
    $string .='<span>'.esc_html($saturday_text).'</span>';
    $string .='</div>';
    $string .='</div>';
    }
    if(($sunday_title && $sunday_text) != ""){
    $string .='<div class="opening-hours-item">';
    
    if($opening_hours_icon != ''){
            $string .= '<i class="opening-hours-icon ' .esc_attr($opening_hours_icon).'" style="';
            
            if ($opening_hours_icon_color != "") {
            $string .= ' color:' . esc_attr($opening_hours_icon_color) . ';';
            }
            if ($opening_hours_icon_size != "") {
            $string .= ' font-size:' . esc_attr($opening_hours_icon_size) . 'px;';
            }
            
            $string .= '"></i>';
        }
    
    $string .='<div class="opening-hours-day" style="';
    
    if($opening_hours_text_color != ""){
        $string .= ' color:'.esc_attr($opening_hours_text_color).';';
    }
    if($opening_hours_title_size != ""){
        $string .= ' font-size:'.esc_attr($opening_hours_title_size).'px;';
    }
    
    $string .='">';
    $string .='<span>'.esc_html($sunday_title).'</span>';
    $string .='</div>';   
    $string .='<div class="opening-hours-time"  style="';
    
    if($opening_hours_text_color != ""){
        $string .= ' color:'.esc_attr($opening_hours_text_color).';';
    }
    if($opening_hours_hours_size != ""){
        $string .= ' font-size:'.esc_attr($opening_hours_hours_size).'px;';
    }
    
    $string .='">';
    $string .='<span>'.esc_html($sunday_text).'</span>';
    $string .='</div>';
    $string .='</div>';
    }
    
    
    
    $string .= '</div>';


    return $string;
}

