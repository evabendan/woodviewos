<?php

add_action('vc_before_init', 'dental_care_team_members_VC');

function dental_care_team_members_VC() {
    vc_map(array(
        "name" => esc_html__("Team Members", 'dental-care'),
        "base" => "dental_care_team_members",
        "class" => "",
        "category" => esc_html__('Dental Care', 'dental-care'),
        "params" => array(
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Title", 'dental-care'),
                "param_name" => "title",
                "description" => esc_html__("Title text Here. Leave blank if no title is needed.", 'dental-care')
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Image Width", 'dental-care'),
                "param_name" => "img_width",
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Image Height", 'dental-care'),
                "param_name" => "img_height",
            ),
            array(
                "type" => "dropdown",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Team Display Type", 'dental-care'),
                "param_name" => "team_type",
                "description" => esc_html__("Choose a team type.", 'dental-care'),
                "value" => array("" => "", "Team Carousel" => "team_carousel", "Team Grid" => "team_grid"),
            ),
            array(
                "type" => "dropdown",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Order by title", 'dental-care'),
                "param_name" => "order_items",
                "description" => esc_html__("Choose if to order items", 'dental-care'),
                "value" => array(
                    '' => '',
                    'Yes' => 'yes',
                    'No' => 'no',
                )
            ),
            array(
                "type" => "dropdown",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Enable Social ", 'dental-care'),
                "param_name" => "social_en",
                "description" => esc_html__("Choose to enable or disable team member social links.", 'dental-care'),
                "value" => array(
                    '' => '',
                    'On' => 'on',
                    'Off' => 'off',
                )
            ),
            array(
                "type" => "dropdown",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Enable Team Links ", 'dental-care'),
                "param_name" => "links_en",
                "description" => esc_html__("Choose to enable or disable team member links.", 'dental-care'),
                "value" => array(
                    '' => '',
                    'On' => 'on',
                    'Off' => 'off',
                )
            ),
            array(
                "type" => "dropdown",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Enable Detail Overlay ", 'dental-care'),
                "param_name" => "overlay_en",
                "description" => esc_html__("Choose to enable or disable the details overlay.", 'dental-care'),
                "value" => array(
                    '' => '',
                    'On' => 'on',
                    'Off' => 'off',
                )
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Number of items", 'dental-care'),
                "param_name" => "num_items",
                "description" => esc_html__("Enter the number of team members to display. Enter -1 to display all items.", 'dental-care')
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Carousel Speed", 'dental-care'),
                "param_name" => "carousel_speed",
                "description" => esc_html__("Enter the number for the carousel speed. (Default: 5000)", 'dental-care')
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Number of carousel items", 'dental-care'),
                "param_name" => "carousel_items",
                "description" => esc_html__("Enter the number of team members columns to display in carousel.", 'dental-care')
            ),
            array(
                "type" => "dropdown",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Enable Arrows", 'dental-care'),
                "param_name" => "arrows_en",
                "description" => esc_html__("Choose to enable or disable arrows on carousel.", 'dental-care'),
                "value" => array(
                    '' => '',
                    'On' => 'on',
                    'Off' => 'off',
                ),
                
            ),
        )
    ));
}

function dental_care_team_members_shortcode($atts, $content = NULL) {
    global $post;
    extract(shortcode_atts(array(
        'param' => '',
        'title' => '',
        'num_items' => ' ',
        'carousel_speed' => '',
        'team_type' => '',
        'carousel_items' => '',
        'social_en' => '',
        'links_en' => '',
        'overlay_en' => '',
        'order_items' => '',
        'arrows_en' => '',
        'img_width' => '',
        'img_height' => ''
                    ), $atts));

    if ($num_items == NULL) {
        $num_items = 3;
    }
    
    if ($order_items == 'yes'){       
         $args = array(
        'post_type' => 'team-member',
        'post_status' => 'publish',
        'pagination' => true,
        'orderby' => 'title',
	'order'   => 'ASC',
        'posts_per_page' => $num_items,   
    );
    }else{
     $args = array(
        'post_type' => 'team-member',
        'post_status' => 'publish',
        'pagination' => true,
        'posts_per_page' => $num_items,
    );
    }
  

    // The Query
    $query = new WP_Query($args);
    $string = '<div class="dental-care-team-wrapper">';
    
    if ($arrows_en == 'on') {       
            $string .= '<div class="carousel_arrow_nav_top">';           
            $string .= '<a class="btn arrow_prev_top"><i class="fa fa-chevron-left"></i></a>';
            $string .= '<a class="btn arrow_next_top"><i class="fa fa-chevron-right"></i></a>';
            $string .= '</div>';       
    }
    
    if ($title != NULL) {
        $string .= '<h3 class="dental-care-VC-title">' . esc_html($title) . '</h3>';
    }

    if ($team_type == "team_carousel") {

        $postcount = $query->post_count;
        dental_care_add_team_members_carousel($carousel_speed, $carousel_items, $postcount);

        $string .= '<div class="dental-care-team-members-carousel owl-carousel">';

        while ($query->have_posts()) {
            $query->the_post();

            $dental_care_team_member_pos = get_post_meta($post->ID, 'team_member_pos', $single = true);
            $dental_care_team_member_social = get_post_meta($post->ID, 'team_member_social_list', $single = true);
            
            if($img_width != "" && $img_height != ""){
               $team_members_img = get_the_post_thumbnail($post->ID,  array( $img_width, $img_height));
            }else{
               $team_members_img = get_the_post_thumbnail($post->ID, 'dental-care-block-thumb');
            }

            $string .= '<div class="team-member-block">';
            if ($team_members_img != NULL){
            $string .= '<div class="team-member-block-img">';
            if($links_en == "off"){
            $string .= $team_members_img;
            }else{
            $string .= '<a rel="external" href="' . get_the_permalink() . '">' . $team_members_img . '<br /></a>';  
            }
            
            if($overlay_en == "on" || $overlay_en == ""){
            $string .= '<div class="team-member-block-img-overlay">';
            if ($social_en == 'off'){
              $string .= '<a rel="external" href="'.get_the_permalink().'">';
              $string .= '<i class="fa fa-link"></i></a>';                      
                                                                                                    
            }else{
            $string .= '<div class="team-member-block-social">';

            if (!empty($dental_care_team_member_social)) {
                $string .= '<ul class="team-member-block-social-list social-icons-list">';
                foreach ($dental_care_team_member_social as $socialnetwork) {
                    if (isset($socialnetwork['title']) && !empty($socialnetwork['title'])) {
                        $title = $socialnetwork['title'];
                    } else
                        $title = '';
                    if (isset($socialnetwork['team_member_social_link']) && !empty($socialnetwork['team_member_social_link'])) {
                        $link = $socialnetwork['team_member_social_link'];
                    } else
                        $link = '';
                    if (isset($socialnetwork['title']) && !empty($socialnetwork['title']) && isset($socialnetwork['team_member_social_link']) && !empty($socialnetwork['team_member_social_link'])) {
                        $string .= '<li>'
                                . '<a class="" href="' . esc_url($link) . '" target="_blank" title="' . esc_attr($title) . '"></a>'
                                . '</li>';
                    }
                }
                $string .= '</ul>';
            }

            $string .= '</div>';
            }
            $string .= '</div>';
            }
            $string .= '</div>';
           }

            $string .= '<div class="team-member-main-detail">';
            if($links_en == "off"){
            $string .= '<h5 class="team-member-main-name">' . get_the_title() . '</h5>';
            }else{
            $string .= '<h5 class="team-member-main-name"><a href="' . get_the_permalink() . '">' . get_the_title() . '</a></h5>';    
            }
            $string .= ' <h6 class="team-member-main-pos">' . esc_html($dental_care_team_member_pos) . '</h6>';
            $string .= ' </div> ';

            $string .= ' </div>';
        }
        $string .= ' </div>';
    } else if ($team_type == "team_grid") {

        $team_member_count = 3;

        while ($query->have_posts()) {
            $query->the_post();

            $dental_care_team_member_pos = get_post_meta($post->ID, 'team_member_pos', $single = true);
            $dental_care_team_member_social = get_post_meta($post->ID, 'team_member_social_list', $single = true);
            
            if($img_width != "" && $img_height != ""){
               $team_members_img = get_the_post_thumbnail($post->ID,  array( $img_width, $img_height));
            }else{
               $team_members_img = get_the_post_thumbnail($post->ID, 'dental-care-block-thumb');
            }

            if ($team_member_count == 3) {
                $team_member_count = 0;
                $string .= '<div class="row">';
            }
            $team_member_count++;

            $string .= '<div class="col-md-4 team-member-block-item">';
            $string .= '<div class="team-member-block">';
            if (get_the_post_thumbnail() != NULL){
            $string .= '<div class="team-member-block-img">';
            if($links_en == "off"){
               $string .=  $team_members_img;   
            }else{
               $string .= '<a rel="external" href="' . get_the_permalink() . '">' . $team_members_img . '<br /></a>';     
            }
            
            if($overlay_en == "on" || $overlay_en == ""){
            $string .= '<div class="team-member-block-img-overlay">';
              if ($social_en == 'off'){
              $string .= '<a rel="external" href="'.get_the_permalink().'">';
              $string .= '<i class="fa fa-link"></i></a>';                      
                                                                                                    
            }else{
            $string .= '<div class="team-member-block-social">';

            if (!empty($dental_care_team_member_social)) {
                $string .= '<ul class="team-member-block-social-list social-icons-list">';
                foreach ($dental_care_team_member_social as $socialnetwork) {
                    if (isset($socialnetwork['title']) && !empty($socialnetwork['title'])) {
                        $title = $socialnetwork['title'];
                    } else
                        $title = '';
                    if (isset($socialnetwork['team_member_social_link']) && !empty($socialnetwork['team_member_social_link'])) {
                        $link = $socialnetwork['team_member_social_link'];
                    } else
                        $link = '';
                    if (isset($socialnetwork['title']) && !empty($socialnetwork['title']) && isset($socialnetwork['team_member_social_link']) && !empty($socialnetwork['team_member_social_link'])) {

                        $string .= '<li>';
                        $string .= '<a class="" href="' . esc_url($link) . '" target="_blank" title="' . esc_attr($title) . '"></a>';
                        $string .= '</li>';
                    }
                }
                $string .= '</ul>';
            }

            $string .= '</div>';
            }
            $string .= '</div>';
            }
            $string .= '</div>';
            }

            $string .= '<div class="team-member-main-detail">';
            if($links_en == "off"){
            $string .= '<h5 class="team-member-main-name">' . get_the_title() . '</h5>';
            }else{
            $string .= '<h5 class="team-member-main-name"><a href="' . get_the_permalink() . '">' . get_the_title() . '</a></h5>';    
            }            $string .= '<h6 class="team-member-main-pos">' . esc_html($dental_care_team_member_pos) . '</h6>';
            $string .= '</div></div></div>';

            if ($team_member_count == 3) {
                $string .= '</div>';
            }
        }
        if ($team_member_count < 3) {
            $string .= '</div>';
        }
    }
    $string .= ' </div> ';
    wp_reset_postdata();
    return $string;
}

/**
 * Add Carousel Settings
 */
function dental_care_add_team_members_carousel($carousel_speed = NULL, $carousel_items = 3, $postcount = NULL) {
    echo '<script>';
    echo 'jQuery(document).ready(function($) {';
    echo '"use strict";';
    echo '$(".dental-care-team-members-carousel").owlCarousel({';
    if ($postcount > 1) {
        echo 'loop: true,';
    } else {
        echo 'loop: false,';
    }
    echo ' 
            margin: 30,
            autoplay: true,
            autoplayHoverPause: true,
            navigation: false,
            dots: false,';
    if ($carousel_speed == NULL || !isset($carousel_speed)) {
        echo 'autoplayTimeout: 5000,';
    } else {
        echo 'autoplayTimeout: ' . esc_html($carousel_speed) . ',';
    }
    echo'
            items: ' . esc_html($carousel_items) . ',
            responsiveClass:true,
            responsive:{
            0:{
            items:1,           
             },
            800:{
            items:2,            
            },
            1100:{
            items:' . esc_html($carousel_items) . ', 
            }
            }
                });
                
     $(".dental-care-team-wrapper .arrow_next_top").click(function(){
     $(".dental-care-team-members-carousel").trigger("next.owl.carousel");
     })
     $(".dental-care-team-wrapper .arrow_prev_top").click(function(){
     $(".dental-care-team-members-carousel").trigger("prev.owl.carousel");
     })

    });';
    echo '</script>';
}

add_action('wp_footer', 'dental_care_add_team_members_carousel', 10, 2);
