<?php

add_action('vc_before_init', 'dental_care_info_icon_VC');

function dental_care_info_icon_VC() {
    vc_map(array(
        "name" => esc_html__("Info Icon", 'dental-care'),
        "base" => "dental_care_info_icon",
        "class" => "",
        "category" => esc_html__('Dental Care', 'dental-care'),
        "params" => array(
            array(
                "type" => "dropdown",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Dental Icon", 'dental-care'),
                "param_name" => "dental_icon_select",
                "description" => esc_html__("Choose a dental icon. See the Icon section of the theme documentation.", 'dental-care'),
                "value" => array(
                    "" => "",
                    "Anesthesia" => "icon-anesthesia ",
                    "Braces " => "icon-braces",
                    "Brush Teeth " => "icon-brush-teeth",
                    "Caries Defence " => "icon-caries-defence",
                    "Certificate" => "icon-certificate",
                    "Cleaned Tooth" => " icon-cleaned-tooth",
                    "Cracked Tooth" => "icon-cracked-tooth",
                    "Dental Calculus" => "icon-dental-calculus",
                    "Dental Care" => "icon-dental-care",
                    "Dental Caries" => "icon-dental-caries",
                    "Dentist" => "icon-dentist",
                    "Dentist Report " => "icon-dentist-report",
                    "Healthy Tooth" => "icon-healthy-tooth",
                    "Label" => "icon-label",
                    "Medical Records" => "icon-medical-records",
                    "Medical Report " => "icon-medical-report ",
                    "Parodontosis" => "icon-parodontosis",
                    "Parodontosis A" => "icon-parodontosis-a",
                    "Patient Card" => "icon-patient-card",
                    "Pin Tooth" => "icon-pin-tooth",
                    "Protection" => "icon-protection",
                    "Teeth" => "icon-teeth",
                    "Toothbrush" => "icon-toothbrush",
                    "Tooth Seal" => "icon-tooth-seal",
                    "X-Ray" => "icon-x-ray",
                ),
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Icon Class", 'dental-care'),
                "param_name" => "icon_class",
                "description" => esc_html__("Enter an icon class. Also supports Font Awesome e.g. fa fa-check., See icons", "dental-care") . " <a href='https://fortawesome.github.io/Font-Awesome/icons/' target='_blank'>here</a>",
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Icon Font Size", 'dental-care'),
                "param_name" => "icon_font_size",
                "value" => 50,
                "min" => 1,
                "max" => 100,
                "suffix" => "px",
                "description" => esc_html__("Enter icon font size.", 'dental-care')
            ),
            array(
                "type" => "colorpicker",
                "class" => "",
                "heading" => esc_html__("Icon Color", 'dental-care'),
                "param_name" => "icon_color",
                "description" => esc_html__("Choose icon color", 'dental-care')
            ),
            array(
                "type" => "dropdown",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Icon Position", 'dental-care'),
                "param_name" => "icon_position_select",
                "description" => esc_html__("Choose an icon position.", 'dental-care'),
                "value" => array("" => "", "Icon Top" => "icon_top", "Icon Left" => "icon_left"),
            ),
            array(
                "type" => "vc_link",
                "class" => "",
                "heading" => __("Link", "dental-care"),
                "param_name" => "icon_link",
                "value" => "",
                "description" => __("Choose a link for the info icon.", "dental-care"),
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Title", 'dental-care'),
                "param_name" => "icon_title",
                "description" => esc_html__("Enter a title", 'dental-care'),
                "group" => "Content"
            ),
            array(
                "type" => "textarea",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Description", 'dental-care'),
                "param_name" => "icon_desc",
                "description" => esc_html__("Enter a description", 'dental-care'),
                "group" => "Content"
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Title Font Size", 'dental-care'),
                "param_name" => "icon_title_font_size",
                "min" => 1,
                "max" => 100,
                "suffix" => "px",
                "description" => esc_html__("Enter title font size.", 'dental-care'),
                "group" => "Typography"
            ),
            array(
                "type" => "colorpicker",
                "class" => "",
                "heading" => esc_html__("Title Color", 'dental-care'),
                "param_name" => "icon_title_color",
                "description" => esc_html__("Choose a color for the title.", 'dental-care'),
                "group" => "Typography"
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Description Font Size", 'dental-care'),
                "param_name" => "icon_desc_font_size",
                "min" => 1,
                "max" => 100,
                "suffix" => "px",
                "description" => esc_html__("Enter description font size.", 'dental-care'),
                "group" => "Typography"
            ),
            array(
                "type" => "colorpicker",
                "class" => "",
                "heading" => esc_html__("Description Color", 'dental-care'),
                "param_name" => "icon_desc_color",
                "description" => esc_html__("Choose a color for the description.", 'dental-care'),
                "group" => "Typography"
            ),
        )
    ));
}

function dental_care_info_icon_shortcode($atts, $content = NULL) {
    global $post;
    extract(shortcode_atts(array(
        'param' => '',
        'dental_icon_select' => '',
        'icon_class' => '',
        'icon_font_size' => '',
        'icon_class' => '',
        'dental_icon_select' => '',
        'icon_class' => '',
        'icon_color' => '',
        'icon_position_select' => '',
        'icon_bg_color' => '',
        'icon_link' => '',
        'icon_title' => '',
        'icon_desc' => '',
        'icon_title_font_size' => '',
        'icon_title_color' => '',       
        'icon_desc_font_size' => '',
        'icon_desc_color' => ''
                    ), $atts));

    $info_icon = '';
    $icon_position = '';
    $content_position = '';
    $href['url'] = '';

    if ($icon_link != '') {
        $href = vc_build_link($icon_link);
    }

    if ($dental_icon_select != '') {
        $info_icon = $dental_icon_select;
    } else if ($icon_class != '') {
        $info_icon = $icon_class;
    }

    if ($icon_position_select == 'icon_left') {
        $icon_position = 'float: left; width: 15%; padding-right:10px;';
        $content_position = 'float: left; width: 80%; text-align: left; padding: 20px 0;';
    }

    $string = '<div class="stronghold-info-icon-wrapper" style="';
    
    if($icon_position_select == 'icon_left'):
        $string .= 'overflow: auto;';
    endif;
    
    $string .= '">';

    $string .= '<div class="stronghold-info-icon" style="';
    if ($icon_position_select == 'icon_left'):
        $string .= esc_attr($icon_position);
    endif;
    $string .= '">';
    $string .= '<i class="' . esc_attr($info_icon) . '" style="font-size:' . esc_attr($icon_font_size) . 'px;';
    
    if($icon_color != ''):
        $string .= ' color:' . esc_attr($icon_color) . ';';
    endif;
    
    if($icon_bg_color != ''):
        $string .= ' background:' . esc_attr($icon_bg_color) . ';';
    endif;  
    
    $string .= '"></i>';
    $string .= '</div>';

    $string .= '<div class="stronghold-info-icon-content" style="';
    if ($icon_position_select == 'icon_left'):
        $string .= esc_attr($content_position);
    endif;
    $string .= '">';
    $string .= '<div class="stronghold-info-icon-title">';
    if ($href['url'] == '') {
        $string .= '<h3 style="';
        if($icon_title_font_size != ''):
            $string .= 'font-size:' . esc_attr($icon_title_font_size) . 'px;';
        endif;
        
        if($icon_title_color != ''):
            $string .= 'color:' . esc_attr($icon_title_color) . ';';
        endif;       
        $string .= '">';
        
        $string .= esc_html($icon_title) . '</h3>';
    } else {
        $string .= '<a href="' . esc_url($href['url']) . '"><h3 style="';
        if($icon_title_font_size != ''):
            $string .= 'font-size:' . esc_attr($icon_title_font_size) . 'px;';
        endif;
        
        if($icon_title_color != ''):
            $string .= ' color:' . esc_attr($icon_title_color) . ';';
        endif;       
        $string .= '">';
        
        $string .= esc_html($icon_title) . '</h3></a>';
                  
    }
    $string .= '</div>';
    $string .= '<div class="stronghold-info-icon-desc">';
    $string .= '<p style="';
    if ($icon_desc_font_size != ''):
        $string .= 'font-size:' . esc_attr($icon_desc_font_size) . 'px;';
    endif;
    
    if ($icon_desc_color != ''):
        $string .= ' color:' . esc_attr($icon_desc_color) . ';';
    endif;
    
    $string .= '">' . esc_html($icon_desc) . ' </p>';
    $string .= '</div>';
    $string .= '</div>';

    $string .= '</div>';

    return $string;
}
