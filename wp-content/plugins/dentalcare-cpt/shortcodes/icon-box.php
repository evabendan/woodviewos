<?php

add_action('vc_before_init', 'dental_care_icon_box_VC');

function dental_care_icon_box_VC() {
    vc_map(array(
        "name" => esc_html__("Icon Box", 'dental-care'),
        "base" => "dental_care_icon_box",
        "class" => "",
        "category" => esc_html__('Dental Care', 'dental-care'),
        "params" => array(
            array(
                "type" => "dropdown",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Dental Icon", 'dental-care'),
                "param_name" => "dental_icon_select",
                "description" => esc_html__("Choose a dental icon. See the Icon section of the theme documentation.", 'dental-care'),
                "value" => array(
                    "" => "",
                    "Anesthesia" => "icon-anesthesia ",
                    "Braces " => "icon-braces",
                    "Brush Teeth " => "icon-brush-teeth",
                    "Caries Defence " => "icon-caries-defence",
                    "Certificate" => "icon-certificate",
                    "Cleaned Tooth" => " icon-cleaned-tooth",
                    "Cracked Tooth" => "icon-cracked-tooth",
                    "Dental Calculus" => "icon-dental-calculus",
                    "Dental Care" => "icon-dental-care",
                    "Dental Caries" => "icon-dental-caries",
                    "Dentist" => "icon-dentist",
                    "Dentist Report " => "icon-dentist-report",
                    "Healthy Tooth" => "icon-healthy-tooth",
                    "Label" => "icon-label",
                    "Medical Records" => "icon-medical-records",
                    "Medical Report " => "icon-medical-report ",
                    "Parodontosis" => "icon-parodontosis",
                    "Parodontosis A" => "icon-parodontosis-a",
                    "Patient Card" => "icon-patient-card",
                    "Pin Tooth" => "icon-pin-tooth",
                    "Protection" => "icon-protection",
                    "Teeth" => "icon-teeth",
                    "Toothbrush" => "icon-toothbrush",
                    "Tooth Seal" => "icon-tooth-seal",
                    "X-Ray" => "icon-x-ray",
                ),
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Icon Class", 'dental-care'),
                "param_name" => "icon_class",
                "description" => esc_html__("Enter an icon class. Also supports Font Awesome e.g. fa fa-check., See icons", "dental-care") . " <a href='https://fortawesome.github.io/Font-Awesome/icons/' target='_blank'>here</a>",
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Icon Font Size", 'dental-care'),
                "param_name" => "icon_font_size",
                "value" => 50,
                "min" => 1,
                "max" => 100,
                "suffix" => "px",
                "description" => esc_html__("Enter icon font size.", 'dental-care')
            ),
            array(
                "type" => "colorpicker",
                "class" => "",
                "heading" => esc_html__("Icon Color", 'dental-care'),
                "param_name" => "icon_color",
                "description" => esc_html__("Choose icon color", 'dental-care')
            ),
            array(
                "type" => "dropdown",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Enable Flip Effect", 'dental-care'),
                "param_name" => "icon_flip_en",
                "description" => esc_html__("Choose to enable icon box flip effect.", 'dental-care'),
                "value" => array("" => "", "Yes" => "yes", "No" => "no"),
            ),
            array(
                "type" => "vc_link",
                "class" => "",
                "heading" => __("Link", "dental-care"),
                "param_name" => "icon_link",
                "value" => "",
                "description" => __("Choose a link for the icon box.", "dental-care"),
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Title", 'dental-care'),
                "param_name" => "icon_title_front",
                "description" => esc_html__("Enter a title", 'dental-care'),
                "group" => "Front"
            ),
            array(
                "type" => "textarea",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Description", 'dental-care'),
                "param_name" => "icon_desc_front",
                "description" => esc_html__("Enter a description", 'dental-care'),
                "group" => "Front"
            ),
            array(
                "type" => "colorpicker",
                "class" => "",
                "heading" => esc_html__("Text Color", 'dental-care'),
                "param_name" => "icon_text_color_front",
                "description" => esc_html__("Choose a color for the text.", 'dental-care'),
                "group" => "Front"
            ),
            array(
                "type" => "colorpicker",
                "class" => "",
                "heading" => esc_html__("Background Color", 'dental-care'),
                "param_name" => "icon_bg_color_front",
                "description" => esc_html__("Choose a background color or combine it with an image as an overlay.", 'dental-care'),
                "group" => "Front"
            ),
            array(
                "type" => "attach_image",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Background Image", 'dental-care'),
                "param_name" => "icon_bg_img_front",
                "description" => esc_html__("Choose an image for the background.", 'dental-care'),
                "group" => "Front"
            ),
            array(
                "type" => "colorpicker",
                "class" => "",
                "heading" => esc_html__("Background Hover Color", 'dental-care'),
                "param_name" => "icon_bg_hover_color_front",
                "description" => esc_html__("Choose a background hover color.", 'dental-care'),
                "group" => "Front"
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Title", 'dental-care'),
                "param_name" => "icon_title_back",
                "description" => esc_html__("Enter a title", 'dental-care'),
                "group" => "Back"
            ),
            array(
                "type" => "textarea",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Description", 'dental-care'),
                "param_name" => "icon_desc_back",
                "description" => esc_html__("Enter a description", 'dental-care'),
                "group" => "Back"
            ),
            array(
                "type" => "colorpicker",
                "class" => "",
                "heading" => esc_html__("Text Color", 'dental-care'),
                "param_name" => "icon_text_color_back",
                "description" => esc_html__("Choose a color for the text.", 'dental-care'),
                "group" => "Back"
            ),
            array(
                "type" => "colorpicker",
                "class" => "",
                "heading" => esc_html__("Background Color", 'dental-care'),
                "param_name" => "icon_bg_color_back",
                "description" => esc_html__("Choose a background color or combine it with an image as an overlay.", 'dental-care'),
                "group" => "Back"
            ),
            array(
                "type" => "attach_image",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Background Image", 'dental-care'),
                "param_name" => "icon_bg_img_back",
                "description" => esc_html__("Choose an image for the background.", 'dental-care'),
                "group" => "Back"
            ),           
            array(
                "type" => "colorpicker",
                "class" => "",
                "heading" => esc_html__("Button Text Color", 'dental-care'),
                "param_name" => "icon_btn_text_color",
                "description" => esc_html__("Choose a color for the button text.", 'dental-care'),
                "group" => "Button"
            ),
            array(
                "type" => "colorpicker",
                "class" => "",
                "heading" => esc_html__("Button Background Color", 'dental-care'),
                "param_name" => "icon_btn_bg_color",
                "description" => esc_html__("Choose a background color for the button.", 'dental-care'),
                "group" => "Button"
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Title Font Size", 'dental-care'),
                "param_name" => "icon_title_font_size",
                "min" => 1,
                "max" => 100,
                "suffix" => "px",
                "description" => esc_html__("Enter title font size.", 'dental-care'),
                "group" => "Typography"
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => esc_html__("Description Font Size", 'dental-care'),
                "param_name" => "icon_desc_font_size",
                "min" => 1,
                "max" => 100,
                "suffix" => "px",
                "description" => esc_html__("Enter description font size.", 'dental-care'),
                "group" => "Typography"
            ),
        )
    ));
}

function dental_care_icon_box_shortcode($atts, $content = NULL) {
    global $post;
    extract(shortcode_atts(array(
        'param' => '',
        'dental_icon_select' => '',
        'icon_class' => '',
        'icon_font_size' => '',
        'icon_class' => '',
        'dental_icon_select' => '',
        'icon_class' => '',
        'icon_color' => '',
        'icon_bg_color' => '',
        'icon_flip_en' => '',
        'icon_link' => '',
        'icon_title_front' => '',
        'icon_desc_front' => '',
        'icon_text_color_front' => '',
        'icon_bg_color_front' => '',
        'icon_bg_img_front' => '',
        'icon_bg_hover_color_front' => '',
        'icon_title_back' => '',
        'icon_desc_back' => '',
        'icon_text_color_back' => '',
        'icon_bg_color_back' => '',
        'icon_bg_img_back' => '',
        'icon_btn_text_color' => '',
        'icon_btn_bg_color' => '',
        'icon_title_font_size' => '',
        'icon_desc_font_size' => '',
                    ), $atts));

    $info_icon = '';
    $icon_position = '';
    $content_position = '';
    $icon_bg_img_front_src = '';
    $icon_bg_img_back_src = '';
    $href['url'] = '';

    if ($icon_link != '') {
        $href = vc_build_link($icon_link);
    }

    if ($icon_bg_img_front != '') {
        $icon_bg_img_front_src = wp_get_attachment_url($icon_bg_img_front, 'full', false, false);
    }
    if ($icon_bg_img_back != '') {
        $icon_bg_img_back_src = wp_get_attachment_url($icon_bg_img_back, 'full', false, false);
    }

    if ($dental_icon_select != '') {
        $info_icon = $dental_icon_select;
    } else if ($icon_class != '') {
        $info_icon = $icon_class;
    }

    $string = '<div class="'; 
        if ($icon_flip_en == 'no' || $icon_flip_en == ''):
        $string .= 'single-icon-box ';
    endif;
    
    $string .='stronghold-icon-box-wrapper" style="';
    if ($icon_flip_en == 'no' || $icon_flip_en == ''):
        $string .= 'cursor: pointer;';
    endif;
    $string .='"';

    if ($icon_flip_en == 'no' || $icon_flip_en == ''):
        if ($href != ''):
            $string .= ' onclick="javascript:location.href=\'' . esc_attr($href['url']) . '\'"';
        endif;
    endif;
    $string .= '>';
    
    
    $string .= '<div class="stronghold-icon-box-front" style="';
    if ($icon_flip_en == 'no' || $icon_flip_en == ''):
        $string .= 'transform: none;';
    endif;
    if ($icon_bg_color_front != ''):
        $string .= 'background:linear-gradient(
      ' . esc_attr($icon_bg_color_front) . ', 
      ' . esc_attr($icon_bg_color_front) . '
    ) ';
        if ($icon_bg_img_front_src != '') {
            $string .= ',url(' . esc_url($icon_bg_img_front_src) . ') no-repeat center center; background-size:cover;';
        } else {
            $string .= ';';
        }

    endif;
    
   
    $string .= '"';
    
    if($icon_bg_color_front != ''):
        $string .= 'data-color="'.$icon_bg_color_front.'"';   
    endif;
       
    if($icon_bg_hover_color_front != ''):
        $string .= 'data-hoverColor="'.$icon_bg_hover_color_front.'"';   
    endif;
    
    if($icon_bg_img_front_src != ''):
        $string .= ' data-bgImg="'.$icon_bg_img_front_src.'"';   
    endif;
    
   $string .= '>';

    $string .= '<div class="stronghold-info-icon">';
    $string .= '<i class="' . esc_attr($info_icon) . '" style="font-size:' . esc_attr($icon_font_size) . 'px;';
    
    if($icon_color != ''):
        $string .= ' color:' . esc_attr($icon_color) . ';';
    endif;
    
    if($icon_bg_color != ''):
        $string .= ' background:' . esc_attr($icon_bg_color) . ';';
    endif;
   
    $string .= '"></i>';
    $string .= '</div>';

    $string .= '<div class="stronghold-info-icon-content-front">';
    $string .= '<div class="stronghold-info-icon-title">';
    $string .= '<h3 style="';
    
    if($icon_title_font_size != ''):
        $string .= 'font-size:' . esc_attr($icon_title_font_size) . 'px;';
    endif;

    if ($icon_text_color_front != ''):
        $string .= ' color:' . esc_attr($icon_text_color_front) . ';';
    endif;

    $string .= '">' . esc_html($icon_title_front) . ' </h3>';

    $string .= '</div>';
    $string .= '<div class="stronghold-info-icon-desc">';
    $string .= '<p style="';
    
    if($icon_desc_font_size != ''):
        $string .= 'font-size:' . esc_attr($icon_desc_font_size) . 'px;';
    endif;

    if ($icon_text_color_front != ''):
        $string .= ' color:' .esc_attr($icon_text_color_front) . ';';
    endif;

    $string .= '">' . esc_html($icon_desc_front) . ' </p>';
    $string .= '</div>';
    $string .= '</div>';

    $string .= '</div>';

    $string .= '<div class="stronghold-icon-box-back" style="';
    if ($icon_flip_en == 'no' || $icon_flip_en == ''):
        $string .= 'display: none;';
    endif;
    if ($icon_bg_color_back != ''):
        $string .= 'background:linear-gradient(
      ' . esc_attr($icon_bg_color_back) . ', 
      ' . esc_attr($icon_bg_color_back) . '
    ) ';
        if ($icon_bg_img_back_src != '') {
            $string .= ',url(' . esc_url($icon_bg_img_back_src) . ') no-repeat center center; background-size:cover;';
        } else {
            $string .= ';';
        }

    endif;
    $string .= '">';

    $string .= '<div class="stronghold-info-icon-content-back">';
    $string .= '<div class="stronghold-info-icon-title">';

    $string .= '<h3 style="';
    
    if($icon_title_font_size != ''):
        $string .= 'font-size:' . esc_attr($icon_title_font_size) . 'px;';
    endif;
    
    if ($icon_text_color_back != ''):
        $string .= ' color:' . esc_attr($icon_text_color_back) . ';';
    endif;

    $string .= '">' . esc_html($icon_title_back) . ' </h3>';

    $string .= '</div>';
    $string .= '<div class="stronghold-info-icon-desc">';
    $string .= '<p style="';
    
    if($icon_desc_font_size != ''):
        $string .= 'font-size:' . esc_attr($icon_desc_font_size) . 'px;';
    endif;
    

    if ($icon_text_color_back != ''):
        $string .= ' color:' . esc_attr($icon_text_color_back) . ';';
    endif;

    $string .= '">' . esc_html($icon_desc_back) . ' </p>';
    $string .= '</div>';

    $string .= '<div class="stronghold-info-icon-link">';
    if ($href['url'] != '') {
        $string .= '<a class="stronghold-info-icon-link-btn" href="' . esc_url($href['url']) . '" style="';

        if ($icon_btn_text_color != ''):
            $string .= ' color:' . esc_attr($icon_btn_text_color) . ';';
        endif;
        if ($icon_btn_bg_color != ''):
            $string .= ' background:' . esc_attr($icon_btn_bg_color) . ';';
        endif;

        $string .= '">'.esc_html__("Read More", "dental-care").'</a>';
    }
    $string .= '</div>';

    $string .= '</div>';

    $string .= '</div>';

    $string .= '</div>';

    return $string;
}
