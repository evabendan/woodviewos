<?php

/**
 * Plugin Name: Dental Care CPT
 * Plugin URI: 
 * Description: Includes custom post types and Visual Composer Addons for the Dental Care theme.
 * Version: 2.2
 * Author: Stronghold Themes
 * Author URI: 
 * Text Domain: dental-care
 * License: GPL2
 */
 
 define( 'DENTALCARE_DIR', dirname( __FILE__ ) );

 include_once DENTALCARE_DIR . '/post-types/post-type-services.php'; 
 include_once DENTALCARE_DIR . '/post-types/post-type-gallery.php'; 
 include_once DENTALCARE_DIR . '/post-types/post-type-testimonial.php'; 
 include_once DENTALCARE_DIR . '/post-types/post-type-team-members.php'; 
 include_once DENTALCARE_DIR . '/post-types/post-type-brands.php'; 
 include_once DENTALCARE_DIR . '/shortcodes/shortcodes.php'; 
  
 