<div class="field">
	<label class="field-label"><?php esc_html_e("Welcome back, please sign in:","booked"); ?></label>
</div>
	
<div class="field">
	<input value="" placeholder="<?php esc_html_e('Email Address','booked'); ?> ..." class="textfield" id="username" name="username" type="text" >
	<input value="" placeholder="<?php esc_html_e('Password','booked'); ?> ..." class="textfield" id="password" name="password" type="password" >
</div>