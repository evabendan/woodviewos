msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/project\n"
"Last-Translator: \n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"POT-Creation-Date: 2020-11-03T21:05:15+00:00\n"
"PO-Revision-Date: 2020-11-03T21:05:18+00:00\n"
"Language: \n"
"X-Generator: json2po\n"

#. Plugin Name of the plugin
msgid "NextGen"
msgstr ""

#. Plugin URI of the plugin
#. Author URI of the plugin
msgid "https://www.godaddy.com"
msgstr ""

#. Description of the plugin
msgid "Next Generation WordPress Experience"
msgstr "Funkcje WordPressa nowej generacji"

#. Author of the plugin
msgid "GoDaddy"
msgstr ""

#: includes/class-block-editor.php:43
msgid "Setting to disable or enable NextGen administration dashboard shortcut."
msgstr "Ustawienie umożliwiające włączenie lub wyłączenie skrótu do kokpitu administracyjnego NextGen."

#: includes/class-block-editor.php:86
msgid "Headline"
msgstr ""

#: includes/class-block-editor.php:87
msgid "Text"
msgstr ""

#: includes/class-block-editor.php:88
msgid "List"
msgstr ""

#: includes/class-block-editor.php:89
msgid "Image"
msgstr ""

#: includes/class-block-editor.php:90
msgid "Gallery"
msgstr ""

#: includes/class-block-editor.php:91
msgid "Contact"
msgstr ""

#: includes/class-block-editor.php:92
msgid "Call To Action"
msgstr ""

#: includes/class-block-editor.php:129
msgid "WordPress Dashboard"
msgstr "Panel systemu WordPress"
