/*--------------------------------------------------------------
 >>> TABLE OF CONTENTS:
 ----------------------------------------------------------------
 1.0 Mobile Menu
 2.0 Back to top
 3.0 Sticky Header
 4.0 Search Toggle
 5.0 Fit Vids
 6.0 Lightbox
 7.0 Isotope
 8.0 Blog Gallery
 9.0 Blog Grid Gallery Slider
 10.0 Animations
 11.0 Smooth Scrolling 
 12.0 Counter
 13.0 Icon Box
 --------------------------------------------------------------*/

jQuery(document).ready(function ($) {
    "use strict";

    /*--------------------------------------------------------------
     1.0 Mobile Menu
     --------------------------------------------------------------*/
    $('#mobile-menu-toggle-icon').sidr({
        side: 'right',
        speed: 500,
        name: 'mobile-menu',
        source: '.mobile-menu'
    });

    $('body').on("click", function () {
        $.sidr('close', 'mobile-menu');
    });

    /*--------------------------------------------------------------
     2.0 Back to top
     --------------------------------------------------------------*/
    $(window).scroll(function () {
        if ($(this).scrollTop() > 450) {
            $('#to-top').fadeIn();
        } else {
            $('#to-top').fadeOut();
        }
    });
    $('#to-top').click(function () {
        $('html, body').animate({scrollTop: 0}, 1000);
        return false;
    });

    /*--------------------------------------------------------------
     3.0 Sticky Header
     --------------------------------------------------------------*/
    $(window).scroll(function () {
        if ($(this).scrollTop() > 250) {
            $(".sticky-header-wrapper").css({"visibility": "visible", "opacity": "1", "position": "fixed", "top": "0"});
        } else {
            $(".sticky-header-wrapper").css({"visibility": "hidden", "opacity": "0", "top": "-30px"});
        }
    });

    /*--------------------------------------------------------------
     4.0 Search Toggle
     --------------------------------------------------------------*/
    $(".search-toggle").click(function () {
        $(".search-box-wrapper").toggle();
    });

    /*--------------------------------------------------------------
     5.0 FitVids
     --------------------------------------------------------------*/
    $("#main-content").fitVids();
    $("#content").fitVids();

    /*--------------------------------------------------------------
     6.0 Lightboxes
     --------------------------------------------------------------*/
    baguetteBox.run('.gallery-slider', {animation: 'fadeIn', });
    baguetteBox.run('.gallery-carousel', {animation: 'fadeIn', });
    baguetteBox.run('.gallery-justified', {animation: 'fadeIn', });
    baguetteBox.run('.gallery-three-col', {animation: 'fadeIn', });
    baguetteBox.run('.gallery-four-col', {animation: 'fadeIn', });
    baguetteBox.run('.isotope-images-container', {animation: 'fadeIn', });

    $("a[data-rel^='prettyPhoto']").prettyPhoto({
        opacity: 0.95,
        show_title: false,
        social_tools: false
    });

    /*--------------------------------------------------------------
     7.0 Isotope
     --------------------------------------------------------------*/
    var $container = $('.isotope-cat-container');
    $container.imagesLoaded(function () {

        $container.fadeIn(1500).isotope({
            filter: '*',
            itemSelector: '.iso-cat-item',
            layoutMode: 'fitRows',
            transitionDuration: '0.85s',
        });
    });

    var $container2 = $('.isotope-images-container');
    $container2.imagesLoaded(function () {

        $container2.fadeIn(1500).isotope({
            filter: '*',
            itemSelector: '.iso-cat-item',
            layoutMode: 'fitRows',
            transitionDuration: '0.85s',
            percentPosition: true,
            fitRows: {
                columnWidth: 364,
                gutter: 10
            }
        });
    });

    $('.isotope-filter a').click(function () {
        $('.isotope-filter .current').removeClass('current');
        $(this).addClass('current');

        var selector = $(this).attr('data-filter');
        $container2.isotope({
            filter: selector,
        });
        return false;
    });


    /*--------------------------------------------------------------
     8.0 Blog Gallery
     --------------------------------------------------------------*/
    if ($('.gallery-featured-slider').children('.gallery-slide-img').length > 1) {
        var $loopSet = true;
    } else {
        var $loopSet = false;
    }

    $(".gallery-featured-slider").owlCarousel({
        items: 1,
        animateOut: 'fadeOutLeft',
        animateIn: 'fadeInRight',
        autoHeight: false,
        loop: $loopSet,
        autoplay: true,
        autoplayTimeout: 4000,
        nav: true,
        navText: [
            "<i class='fa fa-chevron-left'></i>",
            "<i class='fa fa-chevron-right'></i>"
        ]
    });


    /*--------------------------------------------------------------
     9.0 Blog Grid Gallery Slider
     --------------------------------------------------------------*/
    if ($('.gallery-featured-slider-grid-widget').children('.gallery-slide-img').length > 1) {
        var $loopSet = true;
    } else {
        var $loopSet = false;
    }

    $(".gallery-featured-slider-grid-widget").owlCarousel({
        items: 1,
        animateOut: 'fadeOutLeft',
        animateIn: 'fadeInRight',
        autoHeight: false,
        loop: $loopSet,
        autoplay: true,
        autoplayTimeout: 4000,
        nav: false
    });


    /*--------------------------------------------------------------
     10.0 Animations
     --------------------------------------------------------------*/
    if ($(window).width() > 800) {
        $('.comments-area, .search-content, .above-shop, .widget-area, .single-gallery-wrapper, .service-content, .team-member-main-info, .team-member-additional-details, .team-member-detail, .tech-life-blog-grid .col-md-4, .service-block, .service-block-block-item, .team-member-block-list-img-wrapper, .team-member-block, .dental-care-blog-item, .blog-grid-widget-item-left, .blog-grid-widget-item-right, .dental-care-brands, .dental-care-product-item, .service-block, .dental-care-gallery-widget, .team-member-block, .dental-care-testimonials-item, .article-wrapper, .shop-content, .product_item, .author-box, .related-posts, .company-info-wid, .opening-hours-wid, .recent-widget, .social-widget').viewportChecker({
            classToAdd: 'animated fadeIn',
            offset: 10
        });
    }

    /*--------------------------------------------------------------
     11.0 Smooth Scrolling 
     --------------------------------------------------------------*/
    $(function () {
        $('.masthead a[href*="#"]:not([href="#"]), .header-three .main-navigation a[href*="#"]:not([href="#"])').click(function () {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    $('html,body').animate({
                        scrollTop: target.offset().top - 100
                    }, 1000);
                    return false;
                }
            }
        });
    });

    /*--------------------------------------------------------------
     12.0 Counter
     --------------------------------------------------------------*/
    var runonce = true;
    $('.stronghold-counter-wrapper').waypoint(function () {
        if (runonce == true) {
            $('.counter-number-val').countTo({
                delay: 60,
                speed: 2500,
                refreshInterval: 30,
                time: 4000
            });
            runonce = false;
        }
    }, {offset: '80%', triggerOnce: true});


    /*--------------------------------------------------------------
     13.0 Icon Box
     --------------------------------------------------------------*/
    $(".single-icon-box").hover(function () {

        var frontHcolor = $(this).find('.stronghold-icon-box-front').data('hovercolor');
        var frontbgImg = $(this).find('.stronghold-icon-box-front').data('bgimg');
        var front = $(this).find('.stronghold-icon-box-front');

        if (frontHcolor != "") {

            if (!frontbgImg) {
                frontbgImg = "";
            }

            front.css({
                'background': 'linear-gradient( ' + frontHcolor + ' ,' + frontHcolor + ' ),url(' + frontbgImg + ')  no-repeat center center',
                'background-size': 'cover'
            });
        }

        $(this).css("bottom", "10px");


    }, function () {

        var frontHcolor = $(this).find('.stronghold-icon-box-front').data('hovercolor');
        var frontcolor = $(this).find('.stronghold-icon-box-front').data('color');
        var frontbgImg = $(this).find('.stronghold-icon-box-front').data('bgimg');
        var front = $(this).find('.stronghold-icon-box-front');

        if (frontHcolor != "") {
            if (!frontbgImg) {
                frontbgImg = "";
            }
            front.css({
                'background': 'linear-gradient( ' + frontcolor + ' ,' + frontcolor + ' ),url(' + frontbgImg + ')  no-repeat center center',
                'background-size': 'cover'
            });
        }

        $(this).css("bottom", "0");

    });

});



