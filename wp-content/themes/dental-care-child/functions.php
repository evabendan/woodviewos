<?php

function dental_care_child_scripts() {
if ( is_rtl() ) {
        wp_enqueue_style('dental-care-child-rtl', get_template_directory() . '/rtl.css');
    }
}

add_action('wp_enqueue_scripts', 'dental_care_child_scripts');